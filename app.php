<?php
require_once 'notebook.php';

$db = new PDO('mysql:host=localhost;dbname=notebook', 'root', '');
$db->exec("set names utf8");

$notes = getAll($db);


if (isset($_POST['save_note'])) {
    $title = ($_POST["title"]);
    $note = ($_POST["note"]);

//якщо з скритого поля значення айді не пусте буду сохранять, в іншому випадку додавати нову запись
    $id = !empty($_POST['id']) ? $_POST['id'] : null;

    if (isValid($title) && isValid($note)) {
        if ($id) {
            change($title, $note, $id, $db);
        } else {
            save($title, $note, $db);
        }

    }
}

function isValid($text)
{
    $text = trim($text);
    if (empty($text)) return false;
    return true;
}


if (isset($_GET['note_id'])) {
    $by_id = getById($_GET['note_id'], $db);
}


?> 


