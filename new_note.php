<?php
require_once 'app.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Новая запись</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="col-sm-6 col-sm-offset-3">
        <h1>Новая запись</h1>

        <form action="" method="POST">
            <div class="form-group">
                <input type="hidden" class="form-control" id="id" placeholder="Enter name" name="id"
                       value="<?php if (isset($by_id)) echo $by_id['id'] ?>">
            </div>
            <div class="form-group">
                <label for="usr">Title:</label>
                <input type="text" class="form-control" name="title" id="usr"
                       value="<?php if (isset($by_id)) echo $by_id['title'] ?>" placeholder="Название записи">
            </div>
            <div class="form-group">
                <label for="comment">Note:</label>
                <textarea class="form-control" rows="20" name="note" id="comment" placeholder="Текст записи">
            <?php if (isset($by_id)) echo $by_id['notes'] ?>
        </textarea>
            </div>
            <div class="form-group">
                <button type="submit" name="save_note" class="btn btn-danger col-sm-12">Сохранить</button>
            </div>
        </form>

    </div>
</div>


</body>
</html>

