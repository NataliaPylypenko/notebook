<?php
require_once 'app.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="col-sm-6 col-sm-offset-3">
        <h1>Список записей</h1>
        <div class="comment-wrapper">
            <?php foreach ($notes as $note): ?>
                <p><b><?=  date('d.m.Y') ?></b><a name="view_note" target="_blank" href="http://localhost/notebook/view_note?note_id=<?= $note['id'] ?>"> <?= $note['title'] ?></a></p>
                <p><?= $note['notes'] ?></p>
            <?php endforeach ?>
        </div>

            <a href="http://localhost/notebook/new_note" type="submit" name="add_id" class="btn btn-danger col-sm-12" target="_blank"><b>Добавить запись</b></a>
    </div>
</div>


</body>
</html>
