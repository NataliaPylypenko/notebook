<?php
require_once 'app.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Просмотр записи</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="col-sm-6 col-sm-offset-3">
        <h1><?= $by_id['title'] ?></h1>
        <div class="comment-wrapper">
            <p><b><?= date('d.m.Y') ?></b></p>
            <p><?= $by_id['notes'] ?></p>

            <a href="http://localhost/notebook/new_note?note_id=<?= $by_id['id'] ?>" class="btn btn-danger col-sm-12"><b>Редактировать</b></a>

        </div>
    </div>

</body>
</html>
