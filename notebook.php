<?php

function getAll($db)
{
    /* Выполнение запроса с передачей ему массива параметров */
    $sql = "SELECT * FROM notebook ORDER BY `date` DESC";
    $sth = $db->prepare($sql);
    $sth->execute();
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

function save($title, $note, $db)
{
    $stmt = $db->prepare("INSERT INTO notebook (title, notes) VALUES (:title, :notes)");
    $stmt->bindParam(':title', $title, PDO::FETCH_ASSOC);
    $stmt->bindParam(':notes', $note, PDO::FETCH_ASSOC);
    $stmt->execute();
    return true;
}

function getById($id, $db)
{
    /* Выполнение запроса с передачей ему массива параметров */
    $sql = "SELECT * FROM notebook WHERE id = ?";
    $sth = $db->prepare($sql);
    $sth->execute([$id]);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    return $data;
}

function change($title, $note, $id, $db)
{
    $stmt = $db->prepare("UPDATE `notebook` SET `title`=:title, `notes`=:notes WHERE `id`=:id");
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':notes', $note);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

}
